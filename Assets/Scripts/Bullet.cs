﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {
    private Player m_owningPlayer = null;
    private float gravity = -6.5f;
    private Vector2 m_velocity = new Vector2(15f, 0f);
    private Vector2 levelBounds = new Vector2(12.48f, 6.08f);
    private int m_direction = 0;
    // Use this for initialization
    void Start () {
		
	}

    public void Init(Player owningPlayer)
    {
        float xDir = 0f;
        PlayerController.HorizontalMovement(ref xDir);
        m_owningPlayer = owningPlayer;

        m_direction = xDir < 0 ? -1 : 1;
        m_velocity.x *= m_direction;
    }
	
	// Update is called once per frame
	void Update () {
        UpdateHorizontalMovement();
        UpdateVerticalMovement();

        float x = transform.position.x + m_velocity.x * Time.deltaTime;
        float y = transform.position.y + m_velocity.y * Time.deltaTime;

        transform.position = new Vector3(x, y, 0);

        // Ugly test for now...
        if (transform.position.x > levelBounds.x)
        {
            transform.position = new Vector3(-levelBounds.x, transform.position.y, transform.position.z);
        }

        if (transform.position.x < -levelBounds.x)
        {
            transform.position = new Vector3(levelBounds.x, transform.position.y, transform.position.z);
        }

        if (transform.position.y < -levelBounds.y)
        {
            transform.position = new Vector3(transform.position.x, levelBounds.y, transform.position.z);
        }

        if (transform.position.y > levelBounds.y)
        {
            transform.position = new Vector3(transform.position.x, -levelBounds.y, transform.position.z);
        }
    }

    void UpdateVerticalMovement()
    {
        m_velocity.y += gravity * Time.deltaTime;
    }

    void UpdateHorizontalMovement()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject collObj = collision.gameObject;
        if (collObj.tag == "Block")
        {
            GameController.instance.BulletHitBlock(collObj.GetComponent<Block>(), this);
            Destroy(gameObject);
        }
    }
}
