﻿using UnityEngine;

public abstract class Block : MonoBehaviour {

    public virtual float BlockResistance
    {
        get; protected set;
    }
    public Vector2Int gridPosition = Vector2Int.zero;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	}

    public float TopOfBlock()
    {
        return transform.position.y + GetComponent<BoxCollider2D>().bounds.size.y;
    }

    public float SideOfBlock()
    {
        return transform.position.x + GetComponent<BoxCollider2D>().bounds.size.x;
    }
}
