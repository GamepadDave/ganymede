﻿using UnityEngine;

public class EditorController : MonoBehaviour {
    private const int levelRows = 20;
    private const int levelCols = 40; 
    private Vector2 blockSize = new Vector2(0.64f, 0.64f);
    private Vector2 maxLevelSize = new Vector2(levelCols, levelRows);

    public GameObject blockEmpty;
    public GameObject blockStandard;

    private Block[] blocks;
	// Use this for initialization
	void Start () {
        blocks = new Block[(int)(levelRows * levelCols)];
        Vector2 startPos = new Vector2(-(maxLevelSize.x / 2) * blockSize.x + (blockSize.x / 2), -(maxLevelSize.y / 2) * blockSize.y + (blockSize.y / 2));
        for (int row = 0; row < maxLevelSize.y; row++)
        {
            float yPos = startPos.y + (row * blockSize.y);
            for (int col = 0; col < maxLevelSize.x; col++)
            {
                float xPos = startPos.x + (col * blockSize.x);
                GameObject obj = GameObject.Instantiate(blockStandard, new Vector3(xPos, yPos, 0), Quaternion.identity);
                Block b = obj.GetComponent<Block>();
                b.gridPosition = new Vector2Int(col, row);
                blocks[col * levelRows + row] = b;
            }
        }        		
	}
	
	// Update is called once per frame
	void Update () {
        
    }

    private void FixedUpdate()
    {
        GameObject newBlockObj = null;
        GameObject oldBlockObj = null;

        // Delete block
        if (Input.GetButton("Fire2"))
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
            if (hit.collider != null && hit.collider.gameObject.tag == "Block")
            {
                newBlockObj = GameObject.Instantiate(blockEmpty, new Vector2(hit.transform.position.x, hit.transform.position.y), Quaternion.identity);
                oldBlockObj = hit.collider.gameObject;
            }
        }

        // Create block
        if (Input.GetButton("Fire1"))
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
            if (hit.collider != null && hit.collider.gameObject.tag == "BlockEmpty")
            {
                newBlockObj = GameObject.Instantiate(blockStandard, new Vector2(hit.transform.position.x, hit.transform.position.y), Quaternion.identity);
                oldBlockObj = hit.collider.gameObject;
            }
        }

        if (newBlockObj && oldBlockObj)
        {
            RemoveAddBlock(oldBlockObj.GetComponent<Block>(), newBlockObj.GetComponent<Block>());
        }
    }

    private void RemoveAddBlock(Block remove, Block add)
    {
        add.gridPosition = new Vector2Int(remove.gridPosition.x, remove.gridPosition.y);
        blocks[remove.gridPosition.x * levelRows + remove.gridPosition.y] = add;
        Destroy(remove.gameObject);
    }
}
