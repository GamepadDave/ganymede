﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tests : MonoBehaviour {
    private static string[] buttons =
    {
        "Fire1",
        "Fire2",
        "Fire3",
        "Fire4"
    };

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public static void TestButtons()
    {
        for (int i = 0; i < buttons.Length; i++)
        {
            if (Input.GetButtonUp(buttons[i]))
            {
                Debug.Log(buttons[i]);
            }
        }
    }
}
