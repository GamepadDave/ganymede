﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class CollisionRay
{
    public Vector2 Origin
    {
        private set; get;
    }
    public Vector2 Direction
    {
        private set; get;
    }
    public bool enabled;
    public CollisionRay(Vector2 origin, Vector2 direction)
    {
        Origin = origin;
        Direction = direction;
        enabled = true;
    }
}

[RequireComponent(typeof(Player))]
public class PlayerPhysics : MonoBehaviour {
    private Player player = null;
    public bool hasDoubleJumped = false;
    private Vector2 maxVelocity = new Vector2(8f, 18f);
    public float accelerationSpeed = 0f;
    public enum CollisionSide
    {
        Up,
        Down,
        Left,
        Right,
        None
    }
    public Dictionary<CollisionSide, List<CollisionRay>> rayCheckCollection;
    public enum WallCollision
    {
        None,
        Left,
        Right
    }
    public WallCollision wallCollision;

    // Time to "glue" player to a wall
    private float maxWallStickTime = 0.40f;
    public float currentWallStickTime = 0f;

    // These are public purely for viewing in the Inspector
    public bool onGround = false;
    public float gravity = 0;
    public float gravityWeight = 1.5f;
    public bool changedGravity = false;
    public float maxJumpHeight = 2.6f;
    public float timeToReachPeak = 0.4f;
    public Vector2 velocity = Vector2.zero;
    public float groundResistance = 1f;
    
    void Start()
    {
        wallCollision = WallCollision.None;
        player = GetComponent<Player>();

        rayCheckCollection = new Dictionary<CollisionSide, List<CollisionRay>>();
        rayCheckCollection[CollisionSide.Up] = new List<CollisionRay>
        {
            new CollisionRay(Vector2.zero, Vector2.up),
            new CollisionRay(new Vector2(-player.playerSize.x * 0.25f, 0), Vector2.up),
            new CollisionRay(new Vector2(player.playerSize.x * 0.25f, 0), Vector2.up)
        };
        rayCheckCollection[CollisionSide.Down] = new List<CollisionRay>
        {
            new CollisionRay(Vector2.zero, Vector2.down),
            new CollisionRay(new Vector2(-player.playerSize.x * 0.25f, 0), Vector2.down),
            new CollisionRay(new Vector2(player.playerSize.x * 0.25f, 0), Vector2.down)
        };
        rayCheckCollection[CollisionSide.Left] = new List<CollisionRay>
        {
            new CollisionRay(Vector2.zero, Vector2.left),
            new CollisionRay(new Vector2(0, -player.playerSize.y * 0.25f), Vector2.left),
            new CollisionRay(new Vector2(0, player.playerSize.y * 0.25f), Vector2.left)
        };
        rayCheckCollection[CollisionSide.Right] = new List<CollisionRay>
        {
            new CollisionRay(Vector2.zero, Vector2.right),
            new CollisionRay(new Vector2(0, -player.playerSize.y * 0.25f), Vector2.right),
            new CollisionRay(new Vector2(0, player.playerSize.y * 0.25f), Vector2.right)
        };

        accelerationSpeed = maxVelocity.x * 0.1f;
        gravity = G();
    }

    private void FixedUpdate()
    {
        UpdateMovement();

        CheckForBlockCollisions();
    }

    private void UpdateMovement()
    {
        UpdateHorizontalMovement();
        UpdateVerticalMovement();

        float x = transform.position.x + velocity.x * Time.deltaTime;
        float y = onGround ? transform.position.y : transform.position.y + velocity.y * Time.deltaTime;

        transform.position = new Vector3(x, y, 0);
    }

    private void UpdateHorizontalMovement()
    {
        float xMove = 0f;
        bool playerInput = PlayerController.HorizontalMovement(ref xMove);
        int inputDirection = xMove > 0 ? 1 : -1;
        if (playerInput)
        {
            if (wallCollision != WallCollision.None)
            {
                // Adds some stick to the walls, prevents player from falling off wall immediately after inputting direction
                currentWallStickTime += Time.deltaTime;
                if (currentWallStickTime < maxWallStickTime && !onGround)
                {
                    xMove = 0f;
                }
                else
                {
                    // Temporarily disable collision detection on this side to allow player to leave
                    currentWallStickTime = maxWallStickTime;
                    CollisionSide disabledSide = CollisionSide.None;
                    if (wallCollision == WallCollision.Left && inputDirection == -1)
                    {
                        disabledSide = CollisionSide.Right;
                    }
                    else if (wallCollision == WallCollision.Right && inputDirection == 1)
                    {
                        disabledSide = CollisionSide.Left;
                    }

                    DisableRayChecks(disabledSide);
                }
            }
            else
            {
                SetRayChecks();
            }

            velocity.x += xMove * accelerationSpeed * groundResistance;
        }
        // No input, add some deceleration to their movement
        else if (velocity.x != 0 && wallCollision == WallCollision.None)
        {
            int direction = velocity.x < 0 ? 1 : -1;
            velocity.x += groundResistance * direction;
            if (Mathf.Abs(velocity.x) < 0.5f)
            {
                velocity.x = 0;
            }

            currentWallStickTime = 0f;
        }

        velocity.x = Mathf.Clamp(velocity.x, -maxVelocity.x, maxVelocity.x);
    }

    private void DisableRayChecks(CollisionSide side)
    {
        if (side == CollisionSide.None)
        {
            return;
        }
        lock(rayCheckCollection)
        {
            foreach(CollisionRay ray in rayCheckCollection[side])
            {
                ray.enabled = false;
            }
        }
    }

    private void SetRayChecks()
    {
        foreach (CollisionSide side in Enum.GetValues(typeof(CollisionSide)))
        {
            SetRayChecks(side);
        }
    }

    private void SetRayChecks(CollisionSide side)
    {
        if (side == CollisionSide.None)
        {
            return;
        }
        lock(rayCheckCollection)
        {
            foreach(CollisionRay ray in rayCheckCollection[side])
            {
                ray.enabled = true;
            }
        }
    }

    private void UpdateVerticalMovement()
    {
        if (onGround) return;

        velocity.y += gravity * Time.deltaTime;

        // Give the fall some 'weight'
        if (!changedGravity && velocity.y <= 0f)
        {
            gravity *= gravityWeight;
            changedGravity = true;
        }

        velocity.y = Mathf.Clamp(velocity.y, -maxVelocity.y, maxVelocity.y);
    }

    private void CheckForBlockCollisions()
    {
        float distance = player.playerSize.x * 0.55f;
        bool collision = false;
        int layerMask = 1 << 8; // (player == 8)
        layerMask = ~layerMask;

        lock (rayCheckCollection)
        {
            foreach (CollisionSide side in rayCheckCollection.Keys)
            {
                foreach (CollisionRay ray in rayCheckCollection[side])
                {
                    if (!ray.enabled)
                    {
                        continue;
                    }

                    Vector2 rayOrigin = transform.position + (Vector3)ray.Origin;// new Vector2(transform.position.x + ray.Origin.x, transform.position.y + ray.Origin.y);
                    RaycastHit2D hit = Physics2D.Raycast(rayOrigin, ray.Direction, distance, layerMask);
                    if (hit.collider != null)
                    {
                        collision = true;
                        Vector3 normal = hit.normal;
                        if (hit.collider.gameObject.tag == "Block")
                        {
                            Block block = hit.collider.gameObject.GetComponent<Block>();

                            // Determine which side of the block we collided with
                            normal = hit.transform.TransformDirection(normal);

                            // top
                            if (normal == hit.transform.up)
                            {
                                Debug.DrawRay(rayOrigin, ray.Direction * distance, Color.red);
                                groundResistance = block.BlockResistance;
                                if (!onGround) // Landing from a fall
                                {
                                    SetOnGround(block.TopOfBlock());
                                }
                            }

                            // bottom
                            if (normal == -hit.transform.up)
                            {
                                Debug.DrawRay(rayOrigin, ray.Direction * distance, Color.red);
                                velocity.y = -1f;
                                hasDoubleJumped = true;
                            }

                            // right
                            if (normal == hit.transform.right)
                            {
                                Debug.DrawRay(rayOrigin, ray.Direction * distance, Color.red);
                                SetWallCollision(block.SideOfBlock(), WallCollision.Right);
                            }

                            // left
                            if (normal == -hit.transform.right)
                            {
                                Debug.DrawRay(rayOrigin, ray.Direction * distance, Color.red);
                                SetWallCollision(block.SideOfBlock() - hit.collider.bounds.size.x * 2, WallCollision.Left);
                            }

                            if (normal != hit.transform.right && normal != -hit.transform.right)
                            {
                                wallCollision = WallCollision.None;
                            }
                        }
                    }
                    else
                    {
                        Debug.DrawRay(rayOrigin, ray.Direction * distance, Color.white);
                    }
                }
            }

            if (!collision)
            {
                onGround = false;
                wallCollision = WallCollision.None;
                SetRayChecks();
            }
        }
    }

    // calculates jump velocity
    private float V0()
    {
        return (2 * maxJumpHeight) / (timeToReachPeak);
    }

    // calculates gravity value based on jump "strength"
    private float G()
    {
        return -(2 * maxJumpHeight) / (timeToReachPeak * timeToReachPeak);
    }

    public void StartJump()
    {
        Debug.Log("StartJump");
        if (onGround) // Standard jump
        {
            onGround = false;
            changedGravity = false;
            gravity = G();
            velocity.y = V0();
        }
        else if (wallCollision != WallCollision.None) // Wall jump?
        {
            velocity.y = V0();
            float xVelocity = Mathf.Min(V0(), maxVelocity.x);
            velocity.x = wallCollision == WallCollision.Left ? -xVelocity : xVelocity;
        }
        else if (!hasDoubleJumped)
        {
            velocity.y = V0();
            hasDoubleJumped = true;
        }

        groundResistance = 0.1f;
    }

    private void SetOnGround(float yPos)
    {
        transform.position = new Vector2(transform.position.x, yPos);
        onGround = true;
        velocity.y = 0;
        hasDoubleJumped = false;
    }

    private void SetWallCollision(float xPos, WallCollision collisionSide)
    {
        transform.position = new Vector2(xPos, transform.position.y);
        wallCollision = collisionSide;
        velocity.x = 0f;
    }
}
