﻿using UnityEngine;

public class PlayerController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public static bool HasPressedJump()
    {
        return Input.GetButtonDown("Fire2");
    }

    public static bool IsShootHeld()
    {
        return Input.GetButton("Fire1");
    }

    public static bool HorizontalMovement(ref float value)
    {
        value = Input.GetAxis("Horizontal");
        return !Mathf.Approximately(value, 0f);
    }

    public static float RotationalMovement()
    {
        return Input.GetAxis("Vertical");
    }
}
