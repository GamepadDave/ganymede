﻿using UnityEngine;

[RequireComponent(typeof(PlayerPhysics))]
public class Player : MonoBehaviour
{
    public PlayerPhysics Physics
    {
        private set; get;
    }
    private Vector2 levelBounds = new Vector2(12.48f, 6.08f);
    public Vector2 playerSize = new Vector2(0.64f, 0.64f);
    //public bool outOfBoundsUp = false;
    public GameObject bulletPrefab;
    private float shootTimer = 0.05f;
    private float currentShootInterval = 0f;
    // Use this for initialization
    void Start()
    {
        Physics = GetComponent<PlayerPhysics>();
    }

    // Update is called once per frame
    void Update()
    {
        if (PlayerController.HasPressedJump())
        {
            Physics.StartJump();
        }

        currentShootInterval += Time.deltaTime;
        if (PlayerController.IsShootHeld() && currentShootInterval >= shootTimer)
        {
            GameObject obj = Instantiate(bulletPrefab, transform.position, Quaternion.identity);
            obj.GetComponent<Bullet>().Init(this);
            currentShootInterval = 0f;
        }

        // Ugly test for now...
        if (transform.position.x > levelBounds.x + playerSize.x)
        {
            transform.position = new Vector3(-levelBounds.x, transform.position.y, transform.position.z);
        }

        if (transform.position.x < -levelBounds.x - playerSize.x)
        {
            transform.position = new Vector3(levelBounds.x, transform.position.y, transform.position.z);
        }

        if (transform.position.y < -levelBounds.y - playerSize.y)
        {
            transform.position = new Vector3(transform.position.x, levelBounds.y, transform.position.z);
        }

        if (transform.position.y > levelBounds.y + playerSize.y)
        {
            transform.position = new Vector3(transform.position.x, -levelBounds.y, transform.position.z);
        }
    }
}