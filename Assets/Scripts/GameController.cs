﻿using UnityEngine;

public class GameController : MonoBehaviour
{

    public static GameController instance = null;

    public GameObject blockBasic;
    public GameObject blockIce;
    public Sprite blueBlock;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }


    public void BulletHitBlock(Block block, Bullet bullet)
    {
        block.gameObject.GetComponent<SpriteRenderer>().sprite = blueBlock;
    }

    //Update is called every frame.
    void Update()
    {

    }
}
